from __future__ import print_function
import texttable
class Print:
    C_FAIL = '\033[91m'
    C_OKGREEN = '\033[92m'
    C_ENDC = '\033[0m'

    indent = 0
    @staticmethod
    def print_pass(*string):
	    print(C_OKGREEN + concat(string) + C_ENDC)

    @staticmethod
    def print_fail(*string):
        print(C_FAIL + concat(string) + C_ENDC)

    @staticmethod
    def concat(arr):
        v = ""
        for i in arr:
            v+=i
        return v

    @staticmethod
    def i_print_s(*args, **kwargs):
        '''indented print start'''
        printer = kwargs.get("printer")
        if (printer == None):
            printer = print
        printer(' '*indent + concat(args))
        indent+=2
    
    @staticmethod
    def i_print_d(*args, **kwargs):
        '''indented print detail'''
        printer = kwargs.get("printer")
        if (printer == None):
            printer = print
        printer(' '*indent + concat(args))

    @staticmethod
    def i_print_e(*args, **kwargs):
        '''indented print end'''
        printer = kwargs.get("printer")
        if (printer == None):
            printer = print
        indent-=2
        printer(' '*indent + concat(args))

    @staticmethod
    def err_premature(prog_name, atype='P'):
        print_fail("[[{}]TEST STOPPED]: {}".format(atype, prog_name))

    @staticmethod
    def err_return(prog_name, expected, got, atype='P'):
        i_print_s("[[{}]EXIT]".format(atype), printer=print_fail)
        i_print_d("[TEST]: {}".format(prog_name), printer=print_fail)
        i_print_d("[EXITCODE]: {} != {}".format(expected, got), printer=print_fail)
        i_print_e()

    @staticmethod
    def err_output(prog_name, expected, got, atype='P'):
        i_print_s("[[{}]EXIT]".format(atype), printer=print_fail)
        i_print_d("[TEST]: {}".format(prog_name), printer=print_fail)
        i_print_e()

    @staticmethod
    def err_output_table(program_name, expected_list, got_list, atype='P'):
        i_print_s("[[{}]EXIT]".format(atype), printer=print_fail)
        i_print_d("[TEST]: {}".format(prog_name), printer=print_fail)
        pretty_print(expected_list, got_list, printer=i_print_d,heading="DEBUG_OUTPUT")
        i_print_e()

    @staticmethod
    def column_string(a, b, width):
        '''Returns a coloured string green if a==b and red otherwise'''
        output = ""
        if(not a):
            # Added extra padding to fix for no a present
            output += C_FAIL + " "*width + "|" + b.rjust(width) + C_ENDC
            return output
        if(not b):
            output = a.ljust(width) + "|"
            return output
        if(a == b):
            return C_OKGREEN + a.ljust(width) + "|" + b.rjust(width) + C_ENDC
        else:
            return C_FAIL + a.ljust(width) + "|" + b.rjust(width) + C_ENDC

    @staticmethod
    def pretty_print(alist, blist, ahead="EXPECTED", bhead="BUT GOT", heading="", printer=print):
        bar = lambda s: "="*(s+2)*2
        ia = 0
        ib = 0
        amax = 8
        for i in alist:
            amax = max(amax,len(i))
        for i in blist:
            amax = max(amax, len(i))
        amax = max(amax, len(heading))
        amax += 10
        
        print(bar(amax))
        print(heading.center(amax*2))
        print(bar(amax))
        print(ahead.ljust(amax) + "|" + bhead.rjust(amax))
        print(bar(amax))
        
        while(True):
            if(ia >= len(alist) and ib >= len(blist)):
                break
            if(ia >= len(alist) and ib < len(blist)):
                printer(column_string(None, blist[ib], amax))
                ib+=1
                continue
            if(ib >= len(blist) and ia < len(alist)):
                printer(column_string(alist[ia], None, amax))
                ia+=1
                continue
            printer(column_string(alist[ia], blist[ib], amax))
            ia += 1
            ib += 1
        printer(bar(amax))
