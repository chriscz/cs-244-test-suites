from __future__ import print_function
# Let's get the real printing function
import sys
import os
import subprocess
import texttable


EXECUTABLE  = ''
SOURCE_PATH = ''
TEST_PATH   = ''

class TestRunner():
    program = os.path.join(SOURCE_PATH, 'testhashtable')
    tests = TEST_PATH

    def assertEqual(self, expected, got):
        return expected == got
    def assertNotEquals(self, expected, got):
        return expected != got
    
    def get_prototypes_postfix(path, postfix):
        '''Returns list of (file_path, expected_output)'''
        final = []
        for i in os.listdir(path):
            if (not i.endswith(postfix)): continue
            rp = os.path.join(path, i)
            prototypes = get_prototype_results(rp);
            final.append((rp, prototypes))
        return final

    def get_pass(path, postfix="P.simpl"):
        return get_prototypes_postfix(path, postfix)

    def get_fail(path, postfix="F.simpl"):
        return get_prototypes_postfix(path, postfix)

    def run(self):
        returned = self.test_pass()
        print_pass("*********** PASSED: {} / {}".format(returned[0], returned[1]))

if __name__ == "__main__":
    pre_test()
    test = TestRunner()
    test.run()
