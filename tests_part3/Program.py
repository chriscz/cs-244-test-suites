import subprocess
import sys
import os

class Program:
    def __init__(self, command):
        self.command   = command
        self.PREMATURE = -129987
        self.NO_RUN    = -1000922
        self.init()

    def init(self):
        self.process    = None
        self.returncode = self.NO_RUN 
        self._started   = False 
        self.stdout     = ""
        self.stdin      = ""

    def reset(self):
        self.process.kill()
        self.process = None
    

    def start(self, *args):
        to_run = [program]
        to_run.extend(args)
        self.process = subprocess.Popen(to_run,
                stdin=Popen.PIPE, 
                stderr=Popen.PIPE, 
                stdout=Popen.PIPE)
        self._started = True

    def refresh(self):
        self.out += self.read_out()
        self.err += self.read_err()
    
    def write_wait(*args):
        '''Write and waits for the program to terminate
            this will hang if the program expects input,
            returns the exit code of the program or 
            self.PREMATURE if the exit occured by ctrl-c
        '''
        input = None
        if (len(args)):
            input = ""
            for i in args:
                input+=i
        try: 
            out, err = self.process.communicate(input)
            self.returncode = self.process.returncode
        except KeyboardInterrupt as e:
            self.returncode = self.PREMATURE
            return self.returncode
        self.stdout += out
        self.stderr += err
        return self.returncode

    def read_out(self, size=-1):
        return self.process.stdout.read(size)
 
    def read_err(self, size=-1):
        return self.process.stderr.read(size)

    def write_in(self, *args):
        for i in args:
            self.process.stdin.write(i)
        self.process.stdin.flush()

    def code(self):
        if (self.process and self.returncode == self.NO_RUN):
            self.returncode = self.process.returncode
        return self.returncode

    def x_segfault(self):
        '''Convenience method to check for a segfault exit'''
        return self.returncode == -11

    def x_premature(self):
        '''Convenience method to check for a premature exit'''
        return self.returncode == self.PREMATURE

    def terminate(self):
        self.process.terminate();

    def wait(self):
        self.process.wait()
